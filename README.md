# Steering Behaviours #

Repository for use by Students and others when investigating Game AI basics.

Based on work by: 
* [Anton Pantev](https://github.com/antonpantev/unity-movement-ai/)
* [Steering Behaviors For Autonomous Characters](http://www.red3d.com/cwr/steer/)
* [OpenSteer](http://opensteer.sourceforge.net/)

Anton Pantev's version is for a 2D environment, this version uses 3D coordintates when possible. This gives the from above (not pure 2D) viewpoint, with Y being height above the X-Z plane.