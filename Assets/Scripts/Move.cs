﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{

    public int moveSpeed = 10;
    public Vector3 instantVelocity;

    // Use this for initialization
    void Start ()
    {
        instantVelocity = Vector3.zero;	
    }
	
    // Update is called once per frame
    void Update ()
    {
        // get the position
        Vector3 pos = transform.position;
        float horizontalMovement = Input.GetAxis ("Horizontal");
        float verticalMovement = Input.GetAxis ("Vertical");

        if (horizontalMovement != 0) {
            transform.Translate (transform.right * horizontalMovement * Time.deltaTime * moveSpeed);
        }

        if (verticalMovement != 0) {
			transform.Translate (transform.forward * verticalMovement * Time.deltaTime * moveSpeed);
        }

        instantVelocity = transform.position - pos;
    }




}
