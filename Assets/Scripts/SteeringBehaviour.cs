﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.AccessControl;
using System;
using UnityEngine.VR;

public class SteeringBehaviour : MonoBehaviour
{

	/****************************************************************************************************************
	 * Public attributes
	 ****************************************************************************************************************/

	/// <summary>
	/// AI state: Enumarated list of possible states for this Steering Behaviour.
	/// </summary>
	public enum AIState
	{
		Idle,
		Seek,
		Flee,
		Arrive,
		Pursuit,
		Evade
	}

	/// <summary>
	/// The state the AI is currently in.
	/// </summary>
	public AIState currentState;

	/// <summary>
	/// The target body/object
	/// </summary>
	public Transform target;


	/// <summary>
	/// The move speed of the body
	/// </summary>
	public float maxVelocity = 4.5f;

	/// <summary>
	/// Maximum accelleration for the body
	/// </summary>
	public float maxAccelleration = 5f;


	/// <summary>
	/// The speed to turn towards the target
	/// </summary>
    public float rotationSpeed = 15.0f;

	/// <summary>
	/// The time to reach the target speed.
	/// </summary>
	public float timeToTargetSpeed = 0.1f;

	/// <summary>
	/// The smooth turn to look in direftion of movement (True or False)
	/// </summary>
	public bool smoothLookTurn = true;

	/// <summary>
	/// The number samples for smoothing the turn
	/// </summary>
	public int numSamplesForSmoothing = 3;

	/// <summary>
	/// The values that will be used to turn the AI by.
	/// </summary>
	public Queue<Vector2> velocitySamples = new Queue<Vector2> ();

	/****************************************************************************************************************
	 * Private attributes
	 ****************************************************************************************************************/

	private Rigidbody rb;

	private Vector3 acc;

	/****************************************************************************************************************
	 * Methods for the AI
	 ****************************************************************************************************************/

    void Start ()
    {
		rb = GetComponent < Rigidbody> ();
    }

    void Update ()
    {
        switch (currentState) {
        case AIState.Idle:
            break;
        case AIState.Seek:
			acc=Seek (target.position, maxAccelleration);
            break;
        case AIState.Flee:
            Flee ();
            break;
        case AIState.Arrive:
            Arrive ();
            break;
        case AIState.Pursuit:
            Pursuit ();
            break;
        case AIState.Evade:
            Evade ();
            break;
        }

		if (currentState != AIState.Idle) {
			Steer (acc);
			TurnToFaceWhereYouAreGoing ();
		}
    }

	/****************************************************************************************************************
	 * The AI Type Methods
	 ****************************************************************************************************************/

	/// <summary>
	/// Seek the specified targetPosition with a maxSeekAccelleration.
	/// </summary>
	/// <param name="targetPosition">Target position.</param>
	/// <param name="maxSeekAccelleration">Max seek accelleration.</param>
	public Vector3 Seek (Vector3 targetPosition, float maxSeekAccelleration)
    {
       // Get the direction
		Vector3 accelleration = targetPosition-transform.position;

		// Remove Y
		// accelleration.y = 0;

		// Normalise the accelleration vector
		// then accelleration tot he target
		accelleration.Normalize ();
		accelleration *= maxSeekAccelleration;

		return accelleration;
    }

    void Flee ()
    {
       
    }

    void Arrive ()
    {
        
    }

    void Pursuit ()
    {
        
    }

    void Evade ()
    {
        
    }


	/************************************************************************
	 * Utility Methods
	 */

	/// <summary>
	/// updates the current game object's velocity by using the linear accelleration
	/// </summary>
	/// <param name="linearAccelleration">Linear accelleration.</param>
	public void Steer(Vector3 linearAccelleration) {

		rb.velocity += linearAccelleration * Time.deltaTime;

		if (rb.velocity.magnitude > maxVelocity) {
			rb.velocity = rb.velocity.normalized * maxVelocity;
		}

	}

	/// <summary>
	/// Steer2D - equivalent to Steer but restricted to X/Z
	/// </summary>
	/// <param name="linearAccelleration">Linear accelleration.</param>
	public void Steer2D(Vector2 linearAccelleration) {
		this.Steer(new Vector3(linearAccelleration.x, 0, linearAccelleration.y));
	}

	/* TODO: Fix look in direction /  TurnToFaceWhereYouAreGoing */

	/// <summary>
	/// Turns to face where you are going.
	/// </summary>
	public void TurnToFaceWhereYouAreGoing(){
		Vector3 direction=rb.velocity;
		if (smoothLookTurn) {
			if (velocitySamples.Count == numSamplesForSmoothing) {
				velocitySamples.Dequeue ();
			}

			velocitySamples.Enqueue (rb.velocity);

			foreach (Vector3 v in velocitySamples) {
				direction += v;
			}
			direction /= velocitySamples.Count;
		}

		LookAtDirection(direction);
	}


	/// <summary>
	/// Looks in direction of movement
	/// </summary>
	/// <param name="directionToLook">Direction to look.</param>
	public void LookAtDirection(Vector3 directionToLook){
		float toRotation;
		float rotation;

		directionToLook.Normalize ();

		if (directionToLook.sqrMagnitude > 0.0001f) {
			toRotation = (Mathf.Atan2 (directionToLook.z, directionToLook.x) * Mathf.Rad2Deg);
			rotation = Mathf.LerpAngle (transform.rotation.eulerAngles.y, toRotation, Time.deltaTime * rotationSpeed);
			transform.rotation = Quaternion.Euler (0, rotation, 0);
		} // end if

	} // end LookAtDirection

}
